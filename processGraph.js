import createGraph from 'ngraph.graph';
import saveBinary from 'ngraph.tobinary';
// import saveDot from 'ngraph.todot';
import polylabel from 'polylabel';
import concaveman from 'concaveman';
import Delaunator from 'delaunator';
import cheapRuler from 'cheap-ruler';
import lineIntersect from '@turf/line-intersect';
import buffer from '@turf/buffer';
import { lineString, polygon } from '@turf/helpers';
import { polyfill, h3ToGeo, geoToH3 } from 'h3-js';
import { createWriteStream } from 'fs';
import { performance } from 'perf_hooks';
import cluster from 'cluster';
const numCPUs = require('os').cpus().length;


//TODO: do another pass to process elevators

const ruler = cheapRuler(35.68, 'meters'); // approximate latitude of Tokyo Station

function getExtendedLine(line, extent) {
  const firstCoords = line.geometry.coordinates.slice(0, 2).reverse();
  const firstBearing = ruler.bearing(...firstCoords);
  const newFirstPoint = ruler.destination(firstCoords[1], extent, firstBearing);
  const lastCoords = line.geometry.coordinates.slice(-2);
  const lastBearing = ruler.bearing(...lastCoords);
  const newLastPoint = ruler.destination(lastCoords[1], extent, lastBearing);
  return lineString([newFirstPoint, ...line.geometry.coordinates.slice(1, -1), newLastPoint]);
}

function processSpaces(spaces) {

  const links = [];

  const h3ToGeoJson = (x) => Array.from(h3ToGeo(x)).reverse();
  const nextHalfedge = (e) => (e % 3 === 2) ? e - 2 : e + 1;

  for (const space of spaces) {

    let coords = space.geometry.coordinates;
    if (space.geometry.type === 'MultiPolygon') {
      coords = [concaveman(coords.flat(2), 1)]; // requires Node v11+
    }

    const reference = polylabel(coords, 0.0001);
    const referenceH3 = geoToH3(reference[1], reference[0], 15);

    const h3Set = polyfill(coords, 15, true);
    const h3SetGeo = h3Set.map(x => h3ToGeoJson(x));

    const delaunay = Delaunator.from(h3SetGeo);

    for (let e = 0; e < delaunay.triangles.length; e++) {
      if (e > delaunay.halfedges[e]) { // Only consider one of the possibly two half-edges
        const a = delaunay.triangles[e];
        const b = delaunay.triangles[nextHalfedge(e)];
        const seg = lineString([h3SetGeo[a], h3SetGeo[b]]);
        if (ruler.lineDistance(seg) < 1.1 || !lineIntersect(seg, polygon(coords)).features.length) {
          const aRef = h3Set[a] + "_" + space.id;
          const bRef = h3Set[b] + "_" + space.id;
          links.push({ fromId: aRef, toId: bRef });
        }
      }
    }

    //TODO: give heights to nodes insides stairs/etc

    //TODO: make buffer around end lines, create links btw pts inside space and outside that cross the end line
    const connectedEnds = space.properties.ends.filter(
      end => end.properties.sides.filter(id => id != space.id).length > 0
    );
    //TODO: for spaces too small (polyfill returns nothing), use outer sides
    let ownId = space.id;
    if (!h3Set.length && connectedEnds.length === 2) {
      const otherEnd = connectedEnds.pop();
      ownId = otherEnd.properties.sides.filter(id => id != space.id)[0];
    }
    for (const endLine of connectedEnds) {
      const endLineBuffer = buffer(endLine, 0.000013, { units: 'degrees' }); // ~ 1.17m (Distance in degrees because meters is bogus)
      const endLineBufferContour = endLineBuffer.geometry.coordinates[0];
      const extendedLine = getExtendedLine(endLine, 1.5);
      const firstPoint = extendedLine.geometry.coordinates[0];
      const lastPoint = extendedLine.geometry.coordinates[extendedLine.geometry.coordinates.length - 1];
      const firstPointOnLine = ruler.pointOnLine(endLineBufferContour, firstPoint);
      const lastPointOnLine = ruler.pointOnLine(endLineBufferContour, lastPoint);
      // Turf's buffer coordinates are in clockwise order, geoJson should be counter-clockwise.
      // The buffer's contour begins around the middle of the line on its left side.
      const firstHalf = [
        ...endLine.geometry.coordinates,
        ...endLineBufferContour.slice(0, lastPointOnLine.index + 2).reverse(),
        ...endLineBufferContour.slice(firstPointOnLine.index + 1).reverse(),
        endLine.geometry.coordinates[0]
      ];
      const otherHalf = [
        ...endLine.geometry.coordinates.slice().reverse(),
        ...endLineBufferContour.slice(lastPointOnLine.index + 1, firstPointOnLine.index + 2).reverse(),
        endLine.geometry.coordinates[endLine.geometry.coordinates.length - 1]
      ];
      //TODO: which half is on the space's side?
    }

    // // if no ref because space too small, create links between out sides
    // if (!('reference' in space) && ['B021', 'B025'].includes(space.category)) {
    //   const outerSides = space.ends.map(access => {
    //     const key = Object.keys(access.sides).filter(key => Number(key) !== space.id)[0];
    //     const side = access.sides[key];
    //     return { key: key, side: side };
    //   });

    //   for (const point of outerSides[0].side) {

    //     const pointRef = point + '_' + outerSides[0].key;
    //     const neighbors = hexRing(point, 1);

    //     const otherPoints = outerSides[1].side.filter(pt => neighbors.includes(pt)).map(pt => pt + '_' + outerSides[1].key);

    //     for (const otherPointRef of otherPoints) {
    //       const pointsInOrder = [pointRef, otherPointRef].sort();
    //       links.push({ fromId: pointsInOrder[0], toId: pointsInOrder[1] });
    //     }
    //   }
    // }

    // // if it's an elevator, link with others vertically
    // if ('verticals' in space) {
    //   const elevatorIds = space.verticals[0];
    //   const pointsInside = space.verticals[1];
    //   for (const point of pointsInside) {
    //     const pointRef = point + '_' + space.id;
    //     for (const elevatorId of elevatorIds) {
    //       const otherPointRef = point + '_' + elevatorId;
    //       const pointsInOrder = [pointRef, otherPointRef].sort();
    //       links.push({ fromId: pointsInOrder[0], toId: pointsInOrder[1] });
    //     }
    //   }
    // }

  }

  return links;
}

function saveGraph(graph, networkArr) {
  console.log('Saving...');
  saveBinary(graph);

  const networkArrLookup = {};
  const networkHeightLookup = {};
  for (const i in networkArr) {
    const space = networkArr[i];
    networkArrLookup[space.id] = i;
    networkHeightLookup[space.id] = ('insides' in space) ? Object.keys(space.insides).reduce((acc, height) =>
      space.insides[height].reduce((acc2, pt) => {
        acc2[pt] = height;
        return acc2;
      }, acc), {}) : space.height;
  }

  const logger = createWriteStream('data.json');
  // saveDot.write(graph, (line) => logger.write(line));
  logger.write('[');
  let firstNode = true;
  // write nodes data
  graph.forEachNode(node => {
    if (!firstNode) {
      logger.write(',');
    }
    firstNode = false;
    const pointId = node.id.substring(0, 15);
    const spaceId = node.id.substring(16);
    const space = networkArr[networkArrLookup[spaceId]];
    const data = {};
    if (space != null) {
      data.category = space.category;
      data.height = ('insides' in space) ? networkHeightLookup[spaceId][pointId] : networkHeightLookup[spaceId];
      data.inner = space.inner;
    }
    logger.write(JSON.stringify(data));
  });
  logger.write(']');
  logger.end();

  return new Promise((resolve, reject) => {
    logger.on('finish', () => {
      console.log('Data saved to data.json');
      // process.exit(0);
      resolve();
    });
  });
}


let workers = [];

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);
  const t0 = performance.now();

  const networkData = require('./data/index');
  const networkArr = networkData.network.features;
  const accessArr = networkData.access.features;
  console.log(`Loaded ${networkArr.length} spaces and ${accessArr.length} access lines.`);

  const accessArrLookup = {};
  for (const i in accessArr) {
    accessArrLookup[accessArr[i].id] = i;
  }

  const graph = createGraph();

  const nbWorkers = numCPUs - 1;
  const chunkSize = 2;
  let currentChunkIndex = -1;
  const maxChunkIndex = Math.ceil(networkArr.length / chunkSize) - 1;
  // const maxChunkIndex = 100;
  let nbReturnedChunks = 0;
  let nbLinks = 0;

  const giveChunkToWorker = function (myWorker) {
    if (currentChunkIndex === maxChunkIndex) {  // stop sending
      return;
    }
    currentChunkIndex += 1;
    let slice = networkArr.slice(currentChunkIndex * chunkSize, (currentChunkIndex + 1) * chunkSize);
    // Fetch access lines from the access array
    slice = slice.map(space => {
      if ('ends' in space.properties) {
        space.properties.ends = space.properties.ends.map(end => accessArr[accessArrLookup[end]]);
      }
      return space;
    });
    myWorker.send(slice);
  };

  const onMessage = async function (myWorker, message) {
    giveChunkToWorker(myWorker);
    nbReturnedChunks += 1;
    message.forEach(link => {
      graph.addLink(link.fromId, link.toId);
      nbLinks += 1;
    });
    // console.log(`Master ${process.pid} receives message from worker ${myWorker.process.pid}, now ${nbLinks} links, ${nbReturnedChunks} chunks returned.`);

    if (nbReturnedChunks === maxChunkIndex + 1) {
      // kill workers
      for (var id in cluster.workers) {
        cluster.workers[id].kill();
      }
      await saveGraph(graph, networkArr);
      const t1 = performance.now();
      console.log(`Processing took ${(t1 - t0) / 1000} seconds.`);
      // exit the master process
      process.exit(0);
    }
  };

  // Fork workers
  for (let i = 0; i < nbWorkers; i++) {
    console.log(`Forking process number ${i}...`);

    const worker = cluster.fork();
    workers.push(worker);

    // Listen to messages from worker
    worker.on('message', onMessage.bind(null, worker));
  }

  // Send message to the workers
  workers.forEach(giveChunkToWorker, this);

  cluster.on('exit', function (worker, code, signal) {
    console.log(`Worker ${worker.process.pid} died with code: ${code}, and signal: ${signal}`);
  });

}
else {
  console.log(`Worker ${process.pid} started`);

  process.on('message', function (message) {
    let result = processSpaces(message);
    process.send(result);
  });
}
