import BinaryFile from 'binary-file';
import createGraph from 'ngraph.graph';
import { parse } from 'ngraph.frombinary';
import { aGreedy, aStar, nba } from 'ngraph.path';
import { h3ToGeo, h3Distance } from "h3-js";
import { lineString } from '@turf/helpers';
import cheapRuler from 'cheap-ruler';
import { createWriteStream } from 'fs';
import { performance } from 'perf_hooks';

import savedNetworkData from './save/index';
const labels = savedNetworkData.labels;
const data = savedNetworkData.data;

const linksFile = new BinaryFile('./save/links.bin', 'r');
(async function () {
  try {
    let t0 = performance.now();
    await linksFile.open();
    console.log('Links file opened');
    const length = await linksFile.size();
    console.log(`Links file size: ${length}`);
    const links = await linksFile.read(length);
    await linksFile.close();
    let t1 = performance.now();
    console.log(`Links file closed (took ${(t1 - t0) / 1000} seconds)`);

    t0 = performance.now();
    const graph = Object.assign(new createGraph(), parse(new createGraph(), labels, links));  // the assign is a trick to have the object recognized as a ngraph.graph
    let i = 0;
    let nulls = 0;
    //TODO: what to do when there are null heights?
    for (const label of labels) {
      data[i].h3 = label.substring(0, 15);
      data[i].space = label.substring(16);
      const [lon, lat] = h3ToGeo(data[i].h3).reverse();
      data[i].lon = lon;
      data[i].lat = lat;
      if ('height' in data[i]) {
        data[i].height = Number(data[i].height);
      }
      else {
        nulls += 1;
      }
      graph.addNode(label, data[i]);
      i += 1;
    }
    t1 = performance.now();
    console.log(`Graph created (took ${(t1 - t0) / 1000} seconds)`);
    console.log('nulls:', nulls);
    console.log(graph.getNodesCount());
    console.log(graph.getLinksCount());
    console.log(labels.length);

    // // const logger = fs.createWriteStream('log2.txt');
    // // graph.forEachNode(function(node){
    // //   logger.write(node.id + '\n');
    // // });
    // // logger.close();

    const ruler = cheapRuler(35.68, 'meters');

    const myDistance = (fromNode, toNode, heuristic = false) => {
      const fromPt = [fromNode.data.lon, fromNode.data.lat];
      const toPt = [toNode.data.lon, toNode.data.lat];
      let dist = (heuristic) ? ruler.distance(fromPt, toPt)/1000 : 1;
      // let dist = 1;
      if ('height' in fromNode.data && 'height' in toNode.data) {
        const vDiff = Math.abs(toNode.data.height - fromNode.data.height);
        dist += (toNode.data.category === 'B022') ? vDiff * 1
          : (toNode.data.category === 'B025') ? vDiff * 1
            : (toNode.data.category === 'B021') ? vDiff * 10000000
              : (toNode.data.category === 'B023') ? vDiff * 10000000
                : vDiff;
        if (fromNode.data.inner !== toNode.data.inner) {
          dist += 1000000000;
        }
      }
      return dist;
    };

    const aStar = aGreedy(graph, {
      distance(fromNode, toNode) { return myDistance(fromNode, toNode, false); },
      heuristic(fromNode, toNode) { return myDistance(fromNode, toNode, true); }
    });
    const fromId = '8f2f5a32d8de6b5_3601';
    // const fromId = '8f2f5aade4d1311_1129';
    const toId = '8f2f5aadec0621e_2235';
    // const toId = '8f2f5aade4d1ad1_3554';
    t0 = performance.now();
    const path = aStar.find(fromId, toId);
    t1 = performance.now();
    console.log(`Processing took ${(t1 - t0) / 1000} seconds.`);
    const coords = path.map(node => {
      if (isNaN(node.data.height)) {
        console.log(node.data.id, node.data.height);
      }
      return [node.data.lon, node.data.lat, node.data.height];
    });
    if (coords) {
      const line = lineString(coords);
      const logger = createWriteStream('line.geojson');
      logger.write(JSON.stringify(line));
      logger.close();
    }
  } catch (err) {
    console.log(`There was an error: ${err}`);
  }
})();
